# astron-texmf

LaTeX class that generates documents that look vaguely like ASTRON “standard” MS Word documents.
Comes bundled with a similar template for [ESCAPE Project](https://www.projectescape.eu/) documents.

Provides a bunch of niceties, including Git integration (automatically stamp documents with version numbers, generate change records based on tags, etc) and an extensive (does this mean unwieldy?) set of acronyms.

See the example directory in this repository for a brief example or refer to e.g. [ESCAPE D5.4](https://git.astron.nl/astron-sdc/escape-wp5/docs/escape-d5.4) for a more comprehensive demo.
