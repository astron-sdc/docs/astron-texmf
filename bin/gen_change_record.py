#!/usr/bin/env python3

# Generate a change record for inclusion in an astron-texmf style LaTeX
# document.
# Assumes that all the relevant versions in the document have tags which match
# \d+\.\d+.
import subprocess
from dataclasses import dataclass
from datetime import datetime
from io import StringIO
from re import match


@dataclass
class Tag(object):
    """Represent a git tag"""

    name: str
    message: str

    @property
    def rev(self):
        return subprocess.check_output(["git", "rev-parse", self.name]).decode().strip()

    @property
    def date(self):
        for line in (
            subprocess.check_output(["git", "cat-file", "-p", self.rev])
            .decode()
            .strip()
            .split("\n")
        ):
            if line.startswith("tagger"):
                return datetime.fromtimestamp(int(line.split()[-2]))


def escape_latex(text):
    return text.strip().replace("#", r"\#").replace("&", r"\&")


def get_all_tags():
    try:
        tags = [
            Tag(*tag.split(None, 1))
            for tag in subprocess.check_output(["git", "tag", "-n99", "--merged"])
            .decode()
            .strip()
            .split("\n")
            if tag
        ]
    except:
        tags = []
    return tags


def generate_change_record(tags):
    output = StringIO()
    if tags:
        output.write("\setDocChangeRecord{\n")
        for tag in sorted(tags, key=lambda x: x.name, reverse=True):
            if match(r"\d+\.\d+", tag.name):
                output.write(f"    \\addChangeRecord{{{escape_latex(tag.name)}}}")
                output.write(f"{{{tag.date.strftime('%Y-%m-%d')}}}")
                output.write(f"{{{escape_latex(tag.message)}}}\n")
        output.write("}")
    return output.getvalue()


if __name__ == "__main__":
    print(generate_change_record(get_all_tags()))
