.FORCE:

changes.tex: Makefile .FORCE
	rm -f $@
	printf "%s\n" "% GENERATED FILE -- do not edit" >>$@
	$(TEXMFHOME)/../bin/gen_change_record.py >>$@
