GITVERSION := $(shell git describe --always --dirty)
GITDATE := $(shell git log -1 --date=short --pretty=%ad)
GITFIRSTYEAR := $(shell git log --reverse --date=short --pretty=%ad | head -1 | cut -d- -f1)
GITLASTYEAR := $(shell git log -1 --date=short --pretty=%ad | cut -d- -f1)

.FORCE:

meta.tex: Makefile .FORCE
	rm -f $@
	printf "%s\n" "% GENERATED FILE -- do not edit" >>$@
	printf "%s\n" "\newcommand{\vcsRevision}{$(GITVERSION)}" >>$@
	printf "%s\n" "\newcommand{\vcsDate}{$(GITDATE)}" >>$@
	printf "%s\n" "\newcommand{\vcsFirstYear}{$(GITFIRSTYEAR)}" >>$@
	printf "%s\n" "\newcommand{\vcsLastYear}{$(GITLASTYEAR)}" >>$@
	printf "%s\n" "\newcommand{\docHandle}{$(DOCHANDLE)}" >>$@
