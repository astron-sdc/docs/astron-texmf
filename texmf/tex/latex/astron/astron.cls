% Inspired by:
%
%  https://support.astron.nl/confluence/display/~kruger/Confluence+to+latex+to+pdf
%  https://lsst-texmf.lsst.io
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{astron}[ASTRON document class]

\newlength{\topMargin}
\newlength{\bottomMargin}
\newlength{\sideMargin}
\newlength{\headHeight}
\newlength{\footSkip}
\newlength{\headSep}
\setlength{\topMargin}{3.5cm}
\setlength{\bottomMargin}{3.5cm}
\setlength{\sideMargin}{2cm}
\setlength{\headHeight}{52.05211pt}
\setlength{\footSkip}{56.9055pt}
\setlength{\headSep}{25.0pt}

\newcommand{\changeRecordName}{Document History}

% Handle various document types, adding appropriate warning messages.
% Document type is specified as a class option; if none is specified, we use "undefined".
\newif\if@hastype
\@hastypefalse
\newcommand{\docType}{Undefined}

\DeclareOption{draftwm}{
  \AtEndOfClass{\RequirePackage[firstpageonly]{draftwatermark}}
}

\DeclareOption{note}{
  \newcommand{\docTypeDescription}{
    \fbox{%
      \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
        \begin{center}
          \begin{small}
            This technical note expresses the opinions of the authors on the date of writing.\\
            It is not binding on the SDC Program, and may not accurately reflect future developments.
          \end{small}
        \end{center}
      \end{minipage}%
    }
  }

  \renewcommand{\docType}{Tech. Note}
  \@hastypetrue
}

\DeclareOption{proposed}{
  \newcommand{\docTypeDescription}{
    \fbox{%
      \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
        \begin{center}
          \begin{small}
            This document proposes a change to the technical baseline of the SDC Program.\\
            It is not binding on the Program until approved by a change control process.
          \end{small}
        \end{center}
      \end{minipage}%
    }
  }

  \renewcommand{\docType}{Proposed}
  \@hastypetrue
}

\DeclareOption{baseline}{
  \newcommand{\docTypeDescription}{
    \fbox{%
      \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
        \begin{center}
          \begin{small}
            This document is part of the technical baseline of the SDC Program.\\
            It is binding on the activities of the Program.\\
            It may only be modified through a formal change control process.
          \end{small}
        \end{center}
      \end{minipage}%
    }
  }

  \renewcommand{\docType}{Baseline}
  \@hastypetrue
}

% Default logo
\newcommand{\logo}{\includegraphics[width=8cm]{astron-logo}}

% 75th anniversary logo
\DeclareOption{75}{
  \renewcommand{\logo}{\includegraphics[width=4.85cm]{astron75-logo}}
}

% LOFAR documents (for now, this just means using the LOFAR logo).
\DeclareOption{lofar}{
  \renewcommand{\logo}{\includegraphics[width=6cm]{lofar-logo}}
}

\DeclareOption{lofar-eric}{
  \renewcommand{\logo}{\includegraphics[width=2.15cm]{lofar-eric-logo}}
}

% No logo
\DeclareOption{nologo}{
  \renewcommand{\logo}{}
}

% Short documents don't have a title page; just stick the title next to the logo on the first page.
\newif\if@shortDoc
\@shortDocfalse
\DeclareOption{short}{
  \@shortDoctrue
}

\ProcessOptions\relax

%
% Standard Definitions
%
\input{common}

\newcommand{\docRevision}{Set the document revision with \texttt{\textbackslash{}setDocRevision}}
\newcommand{\setDocRevision}[1]{
  \renewcommand{\docRevision}{#1}
}

\newcommand{\docProgram}{Set the document program with \texttt{\textbackslash{}setDocProgram}}
\newcommand{\setDocProgram}[1]{
  \renewcommand{\docProgram}{#1}
}

%
% Define authors, approvers, etc for the front page
%
\providecommand{\addPerson}[3]{ #1 & #2 & #3 \tabularnewline}

\newcommand{\docAuthors}{
  ... & ... & ... \tabularnewline
}
\newcommand{\setDocAuthors}[1]{\renewcommand{\docAuthors}
  {
    #1
  }}

\newcommand{\docCheckers}{
  ... & ... & ... \tabularnewline
}
\newcommand{\setDocCheckers}[1]{\renewcommand{\docCheckers}
  {
    #1
  }}

\newcommand{\docApprovers}{
  ... & ... & ... \tabularnewline
}
\newcommand{\setDocApprovers}[1]{\renewcommand{\docApprovers}
  {
    #1
  }}

\newcommand{\docAuthorizers}{
  ... & ... & ... \tabularnewline
}
\newcommand{\setDocAuthorizers}[1]{\renewcommand{\docAuthorizers}
  {
    #1
  }}

\newcommand{\authorTable}{%
  \begin{tabularx}{\textwidth}{|p{0.35\textwidth}|p{0.35\textwidth}|X|}
    \cline{2-3} \cline{3-3}
    \multicolumn{1}{c|}{}                                         &
    \textbf{\footnotesize{}Organisatie / Organization}            &
    \textbf{\footnotesize{}Datum / Date} \tabularnewline
    \hline
    \textbf{\footnotesize{}Auteur(s) / Author(s):\vspace{0.2cm}}  &   & \tabularnewline
    \docAuthors
    \hline
    \textbf{\footnotesize{}Controle / Checked:\vspace{0.2cm}}     &   & \tabularnewline
    \docCheckers
    \hline
    \textbf{\footnotesize{}Goedkeuring / Approval:\vspace{0.2cm}} &   & \tabularnewline
    \docApprovers
    \hline
    \textbf{\footnotesize{}Autorisatie / Authorization:}          &   & \tabularnewline
    \docAuthorizers
    \textbf{\footnotesize{}Handtekening / Signature:\vspace{1cm}} &   & \tabularnewline
    \hline
  \end{tabularx}
}

%
% Define headers and footers
%
\pagestyle{fancy}
\fancyhf{}
\lfoot{\thepage \hspace{1pt} / \pageref{LastPage}}
\rfoot{
  \renewcommand{\arraystretch}{1}
  \begin{tabular}{>{\raggedright}ll}
    Doc. Nr.:  & \docNumber{}\tabularnewline
    Rev.:      & \docRevision{}\tabularnewline
    Date:      & \docDate{}\tabularnewline
    Doc. Type: & \docType{}\tabularnewline
  \end{tabular}
}

\rhead{\logo}
\cfoot{\textbf{\large{}\docProgram{}}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}
\footskip2cm

%
% Construct the title page, history, etc.
%

% How far should the title be position from the top of the page?
\newlength{\titleDescent}
\setlength{\titleDescent}{4cm}

\if@shortDoc
  \renewcommand{\maketitle}{
    \begin{textblock*}{0.45\textwidth}(\sideMargin, 0.80cm)
      \fbox{
        \begin{minipage}[c][1.3cm]{\textwidth}
          \centering
          \Huge\textbf{\docTitle{}}
        \end{minipage}
      }
    \end{textblock*}
  }
\else
  \renewcommand{\maketitle}{
    \vspace*{\titleDescent}
    \begin{center}
      \begin{minipage}{0.7\textwidth}
        \centering\LARGE\textbf{\docTitle{}}
      \end{minipage}
    \end{center}

    % This position was determined by trial and error; a bit unsatisfying.
    \newlength{\copyrightBoxPosition}
    \setlength{\copyrightBoxPosition}{25.2cm}

    % Add the copyright notice towards the bottom of the page.
    \begin{textblock*}{1\textwidth}(\sideMargin, \copyrightBoxPosition)
      \fbox{%
        \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
          \begin{center}
            \begin{small}
              \ifthenelse{\equal{\vcsFirstYear}{\vcsLastYear}}
              {\textcopyright{} ASTRON \vcsFirstYear.\\}
              {\textcopyright{} ASTRON \vcsFirstYear--\vcsLastYear.\\}
              All rights are reserved. Reproduction in whole or in part is\\
              prohibited without written consent of the copyright owner.
            \end{small}
          \end{center}
        \end{minipage}%
      }
    \end{textblock*}

    % We generate the author table once (but don't print it) to measure its height,
    % then use that to position the textblock* which ultimately contains the table.
    % Note the measured height is (approximately) half the full height.
    \newlength{\authorTableHalfHeight}
    \settoheight{\authorTableHalfHeight}{\authorTable}

    \if@hastype
      \newlength{\docTypeHalfHeight}
      \settoheight{\docTypeHalfHeight}{\docTypeDescription}
      \begin{textblock*}{1\textwidth}(\sideMargin,
        \dimexpr\copyrightBoxPosition-2\docTypeHalfHeight+\fboxsep)
        \docTypeDescription
      \end{textblock*}

      \begin{textblock*}{1\textwidth}(\sideMargin,
        \dimexpr\copyrightBoxPosition-2\docTypeHalfHeight-2\authorTableHalfHeight+2\fboxsep)
        \authorTable
      \end{textblock*}
    \else
      \begin{textblock*}{1\textwidth}(\sideMargin,
        \dimexpr\copyrightBoxPosition-2\authorTableHalfHeight+\fboxsep)
        \authorTable
      \end{textblock*}
    \fi
    \clearpage
    \changeRecord
  }
\fi
