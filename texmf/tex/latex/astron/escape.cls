\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{escape}[ESCAPE document class]

%
% Basic project details
%
% These refer to ESCAPE, but in principle we could reuse the document class for some other project.
\newcommand{\projectTitle}{European Science Cluster of Astronomy \& Particle physics ESFRI research Infrastructures}
\newcommand{\projectAcronym}{ESCAPE}
\newcommand{\grantAgreement}{824064}
\newcommand{\projectInstrument}{Research and Innovation Action (RIA)}
\newcommand{\projectTopic}{Connecting ESFRI infrastructures through Cluster projects (INFRA-EOSC-4-2018)}
\newcommand{\projectStart}{2019-02-04}
\newcommand{\projectDuration}{42 Months}
\newcommand{\projectWeb}{\href{https://www.projectescape.eu}{www.projectescape.eu}}
\newcommand{\disclaimer}{\projectAcronym{} -- \projectTitle{} has received funding from the European Union’s Horizon 2020 research and innovation programme under the Grant Agreement n° \grantAgreement{}.}

\newcommand{\changeRecordName}{Versioning and contribution history}

%
% Other document metadata
%
% Must be set by the user
\newcommand{\workPackage}{Set the work package with \texttt{\textbackslash{}setWorkPackage}}
\newcommand{\setWorkPackage}[1]{
  \renewcommand{\workPackage}{#1}
}
\newcommand{\leadAuthor}{Set the document author with \texttt{\textbackslash{}setLeadAuthor}}
\newcommand{\setLeadAuthor}[1]{
  \renewcommand{\leadAuthor}{#1}
}
\newcommand{\otherAuthors}{Set the contributing authors with \texttt{\textbackslash{}setOtherAuthors}}
\newcommand{\setOtherAuthors}[1]{
  \renewcommand{\otherAuthors}{#1}
}
\newcommand{\dueDate}{Set the due date with \texttt{\textbackslash{}setDueDate}}
\newcommand{\setDueDate}[1]{
  \renewcommand{\dueDate}{#1}
}
\newcommand{\dueMonth}{Set the due month with \texttt{\textbackslash{}setDueMonth}}
\newcommand{\setDueMonth}[1]{
  \renewcommand{\dueMonth}{#1}
}

%
% Page layout for this document type
%
\newlength{\topMargin}
\newlength{\bottomMargin}
\newlength{\sideMargin}
\newlength{\footSkip}
\newlength{\headSep}
\newlength{\headHeight}
\setlength{\topMargin}{3.5cm}
\setlength{\bottomMargin}{2.5cm}
\setlength{\sideMargin}{1.25cm}
\setlength{\footSkip}{40pt}
\setlength{\headSep}{10pt}
\setlength{\headHeight}{75pt}

%
% Document options set the “dissemination levels”
%
\newcommand{\pu}{\phantom{X}}
\newcommand{\pp}{\phantom{X}}
\newcommand{\re}{\phantom{X}}
\newcommand{\co}{\phantom{X}}

\DeclareOption{pu}{
  \renewcommand{\pu}{X}
}
\DeclareOption{pp}{
  \renewcommand{\pp}{X}
}
\DeclareOption{re}{
  \renewcommand{\re}{X}
}
\DeclareOption{co}{
  \renewcommand{\co}{X}
}
\ProcessOptions\relax

\input{common}

% Default font for ESCAPE documents is Calibri
% But if we can't find it (e.g. in CI), fall back to Arial (if available)
% Attempt to cache the results of the font check; it is very slow
\newcommand{\useArial}{}
\IfFontExistsTF{Arial}{\renewcommand{\useArial}{\setsansfont{Arial}}}{}
\newcommand{\useCalibri}{}
\IfFontExistsTF{Calibri}{\renewcommand{\useCalibri}{\setsansfont{Calibri}}}{\useArial}
\renewcommand{\familydefault}{\sfdefault}
\useCalibri

%
% Define headers and footers
%
\pagestyle{fancy}
\fancyhf{}

\lhead{
  \useArial
  \fontsize{8}{10}\selectfont
  \includegraphics[width=2.0cm]{escape-logo.png}\\
  \docNumber{} -- \docTitle{}}

\rhead{
  \raisebox{1.8cm}{
    \useArial
    \fontsize{8}{10}\selectfont
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{>{\raggedright}ll}
      Project No & \grantAgreement{}\tabularnewline
      Date       & \docDate{}\tabularnewline
    \end{tabular}
  }}

\cfoot{
  \useCalibri
  \begin{tabularx}{\textwidth}{@{}lXll@{}}
    \includegraphics[width=1cm]{flag-eu}\hspace{0.3cm}       &
    \fontsize{8}{10}\selectfont
    \parbox[][1.3cm][t]{14.2cm}{\raggedright{\disclaimer{}}} &
    \parbox[][1.2cm][t]{0.5cm}{\thepage}                     &
    \includegraphics[width=1cm]{escape-galaxy-mind.jpeg}
    \tabularnewline
  \end{tabularx}
}

\renewcommand{\headrulewidth}{0.25pt}
\renewcommand{\footrulewidth}{0pt}

%
% Create the title page
%
% Vertical positions throughout were "eyeballed" — they work ok for a document
% with a title which spans two lines, but will probably break otherwise.
%
\renewcommand{\maketitle}{
  \thispagestyle{empty}

  \newcommand{\printLogo}{
    \begin{center}
      \includegraphics[width=6cm]{escape-logo.png}
    \end{center}
  }

  \newcommand{\projectSummary}{
      {\useArial
      \fontsize{10}{12}\selectfont
      \renewcommand{\arraystretch}{1.5}
      \hyphenpenalty=10000
      \begin{tabularx}{\textwidth}{p{0.25\textwidth}X}
        Project Title         & \projectTitle{}\tabularnewline
        Project Acronym       & \projectAcronym{}\tabularnewline
        Grant Agreement No    & \grantAgreement{}\tabularnewline
        Instrument            & \projectInstrument{}\tabularnewline
        Topic                 & \projectTopic{}\tabularnewline
        Start Date of Project & \projectStart{}\tabularnewline
        Duration of Project   & \projectDuration{}\tabularnewline
        Project Website       & \projectWeb{}\tabularnewline
      \end{tabularx}
      \renewcommand{\arraystretch}{1}}
  }

  \newcommand{\docHeading}{
      \begin{center}
        \begin{minipage}{0.7\textwidth}
          \centering
          \hyphenpenalty=10000
          \LARGE\textbf{\docNumber{} -- \docTitle{}}
        \end{minipage}
      \end{center}
  }

  \newcommand{\docDetails}{
    {\renewcommand{\arraystretch}{1.5}
    \hyphenpenalty=10000
    \begin{tabularx}{\textwidth}{p{0.25\textwidth}X}
      Work Package                               & \workPackage{}\tabularnewline
      Lead Author (Org)                          & \leadAuthor{}\tabularnewline
      \raggedright{Contributing Author(s) (Org)} & \otherAuthors{}\tabularnewline
      Due Date                                   & \dueDate{} (\dueMonth{}) \tabularnewline
      Date                                       & \docDate{} \tabularnewline
      Version                                    & \vcsRevision{} \tabularnewline
    \end{tabularx}
    \renewcommand{\arraystretch}{1}}
  }

  \newcommand{\disseminationLevel}{
    \begin{tabular}{lll}
      \multicolumn{3}{l}{Dissemination Level}\tabularnewline
      \fbox{\pu{}} & PU: & Public\tabularnewline
      \fbox{\pp{}} & PP: & Restricted to other programme participants (including the Commission)\tabularnewline
      \fbox{\re{}} & RE: & Restricted to a group specified by the consortium (including the Commission)\tabularnewline
      \fbox{\co{}} & CO: & Confidential, only for members of the consortium (including the Commission)\tabularnewline
    \end{tabular}
  }

  \vspace*{-2cm}
  \printLogo

  \vspace{0.5cm}
  \projectSummary

  \vspace{0.5cm}
  \docHeading

  \vspace{0.5cm}
  \docDetails

  \begin{textblock*}{1\textwidth}(\sideMargin, 25.5cm)
    \disseminationLevel
  \end{textblock*}

  \clearpage

  \changeRecord
  \section*{Disclaimer}
  \disclaimer{}

  \clearpage
}
